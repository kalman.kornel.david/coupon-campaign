package com.exercise.campaign.validation;

import com.exercise.campaign.validation.domain.RequestValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ControllerAdvice
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidationExceptionHandler {

    public static final String TERRITORY_FIELD_NAME = "territory";
    public static final String TERRITORY_VALIDATION_ERROR_PROPERTY_CODE = "validation.error.territory.invalid";

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(BindException.class)
    ResponseEntity<RequestValidationError> handle(BindException e, Locale locale) {
        RequestValidationError validationError = new RequestValidationError();
        // Enum#valueOf() may cause BindException with user-unfriendly error message
        if (e.hasFieldErrors(TERRITORY_FIELD_NAME)) {
            String errorMessage = messageSource.getMessage(TERRITORY_VALIDATION_ERROR_PROPERTY_CODE, null, locale);
            validationError.addValidationError(TERRITORY_FIELD_NAME, errorMessage);
        } else {
            e.getFieldErrors()
                    .forEach(fieldError -> validationError.addValidationError(fieldError.getField(), fieldError.getDefaultMessage()));
        }
        return ResponseEntity.badRequest()
                .body(validationError);
    }
}
