package com.exercise.campaign.service.domain;

public class CampaignException extends RuntimeException {
    public CampaignException(String errorMessage) {
        super(errorMessage);
    }
}
