-- Values should be obtained from TestConstants.java and test/resources/application.properties
delete from territory_status where territory = 'HUNGARY';
insert into territory_status(territory, number_of_attempts, number_of_winners_today, number_of_winners_overall)
    values('HUNGARY', 4000, 5, 500);
--update territory_status
--    set number_of_attempts = 4000, number_of_winners_today = 5, number_of_winners_overall = 500
--    where territory = 'HUNGARY';