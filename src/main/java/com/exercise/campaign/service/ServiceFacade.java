package com.exercise.campaign.service;

import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.service.domain.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ServiceFacade {

    @Autowired
    private CouponHandler couponHandler;
    @Autowired
    private LimitHandler limitHandler;
    @Autowired
    private WinnerHandler winnerHandler;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Result process(Attempt attempt) {
        couponHandler.handle(attempt);
        limitHandler.handle(attempt);
        final Boolean outcome = winnerHandler.handle(attempt);
        return new Result(outcome);
    }
}
