-- Values should be obtained from TestConstants.java and test/resources/application.properties
delete from territory_status where territory = 'HUNGARY';
insert into territory_status(territory, number_of_attempts, number_of_winners_today, number_of_winners_overall)
    values('HUNGARY', 7, 0, 0);
--update territory_status
--    set number_of_attempts = 7, number_of_winners_today = 0, number_of_winners_overall = 0
--    where territory = 'HUNGARY';