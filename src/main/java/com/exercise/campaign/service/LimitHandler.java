package com.exercise.campaign.service;

import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.repository.TerritoryStatusRepository;
import com.exercise.campaign.service.domain.LimitOfDailyWinnersReachedException;
import com.exercise.campaign.service.domain.LimitOfOverallWinnersReachedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class LimitHandler {
    @Autowired
    private TerritoryStatusRepository territoryStatusRepository;

    @Autowired
    private TerritoryRulesCheck territoryRulesCheck;

    public void handle(Attempt attempt) {
        log.info("Checking territory limits...");

        Optional<TerritoryStatus> optional = territoryStatusRepository.findById(Territory.valueOf(attempt.getTerritory().name()));
        if (!optional.isPresent()) {
            log.debug("Territory information is missing in DB.");
            throw new IllegalStateException("Territory information is missing.");
        }

        TerritoryStatus territoryStatus = optional.get();
        if (territoryRulesCheck.isDailyLimitReached(territoryStatus)) {
            throw new LimitOfDailyWinnersReachedException("Daily limit reached.");
        }
        if (territoryRulesCheck.isOverallLimitReached(territoryStatus)) {
            throw new LimitOfOverallWinnersReachedException("Overall limit reached.");
        }
        updateTerritoryStatus(territoryStatus);
    }

    private void updateTerritoryStatus(TerritoryStatus territoryStatus) {
        territoryStatus.incrementNumberOfAttempts();
        territoryStatusRepository.save(territoryStatus);
    }
}
