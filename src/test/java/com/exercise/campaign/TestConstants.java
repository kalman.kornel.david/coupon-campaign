package com.exercise.campaign;

import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.model.Attempt;

public final class TestConstants {
    public static final String VALID_EMAIL = "my@email.com";
    public static final String INVALID_EMAIL = "not@valid.";
    public static final String VALID_DATE_OF_BIRTH = "2005-11-02";
    public static final String INVALID_DATE_OF_BIRTH = "2010-02-11";
    public static final String VALID_COUPON_CODE = "0ABC456GHI";
    public static final String INVALID_COUPON_CODE = "ABC456";
    public static final String VALID_TERRITORY = "HUNGARY";
    public static final String INVALID_TERRITORY = "AUSTRIA";
    public static final String VALID_ADDRESS = "Sisakos saska utca 7.";

    public static final String EMAIL = "my@mail.com";
    public static final String COUPON_CODE = "couponCode";
    public static final com.exercise.campaign.model.Territory TERRITORY = com.exercise.campaign.model.Territory.HUNGARY;
    public static final Territory TERRITORY_ENTITY = Territory.HUNGARY;

    public static final Attempt ATTEMPT = Attempt.builder()
            .email(EMAIL)
            .couponCode(COUPON_CODE)
            .territory(TERRITORY)
            .build();
}
