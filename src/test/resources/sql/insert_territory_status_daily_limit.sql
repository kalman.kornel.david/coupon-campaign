-- Values should be obtained from TestConstants.java and test/resources/application.properties
delete from territory_status where territory = 'HUNGARY';
insert into territory_status(territory, number_of_attempts, number_of_winners_today, number_of_winners_overall)
    values('HUNGARY', 400, 10, 50);
--update territory_status
--    set number_of_attempts = 400, number_of_winners_today = 10, number_of_winners_overall = 50
--    where territory = 'HUNGARY';
