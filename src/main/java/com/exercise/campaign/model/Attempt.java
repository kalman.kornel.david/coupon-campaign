package com.exercise.campaign.model;

import com.exercise.campaign.validation.MinAge;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@RequiredArgsConstructor
@Getter
@Builder
public class Attempt {
    @NotBlank
    @Email(message = "{validation.error.email}")
    private final String email;

    @MinAge(limit = 13, message = "{validation.error.dateOfBirth}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private final LocalDate dateOfBirth;

    private final String address;

    @NotNull(message = "{validation.error.territory.invalid}")
    private final Territory territory;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9]{10}$", message = "{validation.error.couponCode}")
    private final String couponCode;
}

