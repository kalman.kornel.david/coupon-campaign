package com.exercise.campaign.service.domain;

public class LimitOfOverallWinnersReachedException extends CampaignException {
    public LimitOfOverallWinnersReachedException(String errorMessage) {
        super(errorMessage);
    }
}
