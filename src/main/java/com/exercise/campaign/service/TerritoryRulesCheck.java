package com.exercise.campaign.service;

import com.exercise.campaign.domain.TerritoryStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TerritoryRulesCheck {
    @Autowired
    protected TerritoryRules territoryRules;

    public boolean isDailyLimitReached(TerritoryStatus territoryStatus) {
        return territoryStatus.getNumberOfWinnersToday() >= territoryRules.getRules().get(territoryStatus.getTerritory()).getDailyLimit();
    }

    public boolean isOverallLimitReached(TerritoryStatus territoryStatus) {
        return territoryStatus.getNumberOfWinnersOverall() >= territoryRules.getRules().get(territoryStatus.getTerritory()).getOverallLimit();
    }

    public boolean isWinner(TerritoryStatus territoryStatus) {
        return territoryStatus.getNumberOfAttempts() % territoryRules.getRules().get(territoryStatus.getTerritory()).getWinnerFrequency() == 0;
    }
}
