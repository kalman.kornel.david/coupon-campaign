package com.exercise.campaign.validation.domain;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@Getter
public class RequestValidationError extends ApiError {
    private Map<String, String> validationErrors;

    public RequestValidationError() {
        super(HttpStatus.BAD_REQUEST);
        validationErrors = new HashMap<>();
    }

    public void addValidationError(String fieldName, String errorMessage) {
        validationErrors.put(fieldName, errorMessage);
    }
}
