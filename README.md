Server-side module of the web application to carry out a campaign where customers may participate in a coupon redemption game.
Implemented as a Spring Boot application. Requests are handled via a REST API.
The persistence layer uses H2 database engine.

To build & run the project use Maven and its Spring Boot plugin:
mvn spring-boot:run

When the application is running, the API documentation would be available, by default, at http://localhost:8080/swagger-ui.html
An API descriptor and two sample request (valid and invalid) in JSON format can be found in the project root.
