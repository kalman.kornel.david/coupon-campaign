package com.exercise.campaign.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

public class AgeValidator implements ConstraintValidator<MinAge, LocalDate> {
    private int limit;

    @Override
    public void initialize(MinAge constraintAnnotation) {
        limit = constraintAnnotation.limit();
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        Period period = Period.between(value, LocalDate.now());
        return period.getYears() > limit;
    }
}
