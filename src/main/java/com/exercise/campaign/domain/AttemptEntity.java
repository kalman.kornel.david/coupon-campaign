package com.exercise.campaign.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "attempt")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class AttemptEntity {
    @Id
    private String couponCode;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;

    @Column(nullable = false)
    private LocalDateTime timestamp;
}
