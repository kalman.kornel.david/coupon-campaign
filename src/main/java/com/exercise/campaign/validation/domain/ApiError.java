package com.exercise.campaign.validation.domain;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public class ApiError {
    private final HttpStatus httpStatus;
}
