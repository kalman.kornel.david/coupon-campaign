package com.exercise.campaign.validation;

import com.exercise.campaign.service.domain.CampaignException;
import com.exercise.campaign.service.domain.CouponCodeAlreadyRedeemedException;
import com.exercise.campaign.service.domain.LimitOfDailyWinnersReachedException;
import com.exercise.campaign.service.domain.LimitOfOverallWinnersReachedException;
import com.exercise.campaign.validation.domain.BusinessError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ControllerAdvice
public class BusinessExceptionHandler {

    private static final String COUPON_CODE_MESSAGE_PROPERTY = "processing.error.couponCode";
    private static final String DAILY_LIMIT_MESSAGE_PROPERTY = "processing.error.dailyLimit";
    private static final String OVERALL_LIMIT_MESSAGE_PROPERTY = "processing.error.overallLimit";

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(CouponCodeAlreadyRedeemedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<BusinessError> handle(CampaignException e, Locale locale) {
        final String errorMessage = messageSource.getMessage(COUPON_CODE_MESSAGE_PROPERTY, null, locale);
        return ResponseEntity.badRequest()
                .body(new BusinessError(errorMessage));
    }

    @ExceptionHandler(LimitOfDailyWinnersReachedException.class)
    @ResponseStatus(HttpStatus.ACCEPTED)
    ResponseEntity<BusinessError> handle(LimitOfDailyWinnersReachedException e, Locale locale) {
        final String errorMessage = messageSource.getMessage(DAILY_LIMIT_MESSAGE_PROPERTY, null, locale);
        return ResponseEntity.accepted()
                .body(new BusinessError(errorMessage));
    }

    @ExceptionHandler(LimitOfOverallWinnersReachedException.class)
    @ResponseStatus(HttpStatus.ACCEPTED)
    ResponseEntity<BusinessError> handle(LimitOfOverallWinnersReachedException e, Locale locale) {
        final String errorMessage = messageSource.getMessage(OVERALL_LIMIT_MESSAGE_PROPERTY, null, locale);
        return ResponseEntity.accepted()
                .body(new BusinessError(errorMessage));
    }
}
