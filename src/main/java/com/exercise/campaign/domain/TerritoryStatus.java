package com.exercise.campaign.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "territory_status")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class TerritoryStatus {
    @Id
    @Enumerated(EnumType.STRING)
    private Territory territory;

    @Column(nullable = false)
    private int numberOfAttempts;

    @Column(nullable = false)
    private int numberOfWinnersToday;

    @Column(nullable = false)
    private int numberOfWinnersOverall;

    public void incrementNumberOfAttempts() {
        numberOfAttempts++;
    }

    public void incrementNumberOfWinners() {
        numberOfWinnersToday++;
        numberOfWinnersOverall++;
    }
}

