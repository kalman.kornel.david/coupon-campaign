package com.exercise.campaign.service;

import com.exercise.campaign.domain.Territory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.EnumMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "territory")
@Validated
@Setter
@Getter
public class TerritoryRules {

    public final Map<Territory, Rules> rules = new EnumMap<>(Territory.class);

    @Setter
    @Getter
    static class Rules {
        @NotNull
        @Positive
        private int winnerFrequency;

        @NotNull
        @Positive
        private int dailyLimit;

        @NotNull
        @Positive
        private int overallLimit;
    }
}
