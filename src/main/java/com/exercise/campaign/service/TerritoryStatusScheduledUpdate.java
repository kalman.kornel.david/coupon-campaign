package com.exercise.campaign.service;

import com.exercise.campaign.repository.TerritoryStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(isolation = Isolation.READ_COMMITTED)
public class TerritoryStatusScheduledUpdate {
    @Autowired
    TerritoryStatusRepository territoryStatusRepository;

    @Scheduled(cron = "0 0 0 * * *")
    public void resetDailyNumberOfWinnersAtMidnight() {
        territoryStatusRepository.resetDailyNumberOfWinnersForAllTerritories();
    }
}
