package com.exercise.campaign.api;

import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.service.ServiceFacade;
import com.exercise.campaign.service.domain.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Locale;

@RestController
@RequiredArgsConstructor
public class AttemptController {

    @Autowired
    private final ServiceFacade serviceFacade;

    @PostMapping("/attempt")
    public ResponseEntity<Result> newAttempt(@Valid Attempt attempt, Locale locale) {
        Result result = serviceFacade.process(attempt);
        return ResponseEntity.ok(result);
    }
}
