package com.exercise.campaign.service.domain;

public class CouponCodeAlreadyRedeemedException extends CampaignException {
    public CouponCodeAlreadyRedeemedException(String errorMessage) {
        super(errorMessage);
    }
}
