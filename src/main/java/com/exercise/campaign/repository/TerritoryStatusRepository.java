package com.exercise.campaign.repository;

import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface TerritoryStatusRepository extends JpaRepository<TerritoryStatus, Territory> {
    @Modifying
    @Query("UPDATE territory_status SET numberOfWinnersToday = 0")
    void resetDailyNumberOfWinnersForAllTerritories();
}
