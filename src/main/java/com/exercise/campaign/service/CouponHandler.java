package com.exercise.campaign.service;

import com.exercise.campaign.domain.AttemptEntity;
import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.UserEntity;
import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.repository.AttemptRepository;
import com.exercise.campaign.repository.UserRepository;
import com.exercise.campaign.service.domain.CouponCodeAlreadyRedeemedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Slf4j
public class CouponHandler {
    @Autowired
    private AttemptRepository attemptRepository;

    @Autowired
    private UserRepository userRepository;

    public void handle(Attempt attempt) {
        log.info("Checking coupon validity...");

        Optional<AttemptEntity> attemptEntity = attemptRepository.findById(attempt.getCouponCode());
        if (attemptEntity.isPresent()) {
            throw new CouponCodeAlreadyRedeemedException("Coupon already redeemed.");
        }
        saveNewAttempt(attempt);
    }

    private void saveNewAttempt(Attempt attempt) {
        UserEntity newUserEntity = UserEntity.builder()
                .email(attempt.getEmail())
                .address(attempt.getAddress())
                .dateOfBirth(attempt.getDateOfBirth())
                .territory(Territory.valueOf(attempt.getTerritory().name()))
                .build();
        userRepository.save(newUserEntity);

        AttemptEntity newAttemptEntity = AttemptEntity.builder()
                .couponCode(attempt.getCouponCode())
                .user(newUserEntity)
                .timestamp(LocalDateTime.now())
                .build();
        attemptRepository.save(newAttemptEntity);
    }
}
