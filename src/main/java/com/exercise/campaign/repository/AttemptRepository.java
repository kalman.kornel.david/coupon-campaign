package com.exercise.campaign.repository;

import com.exercise.campaign.domain.AttemptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttemptRepository extends JpaRepository<AttemptEntity, String> {
}
