package com.exercise.campaign.service;

import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.repository.TerritoryStatusRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class WinnerHandler {
    @Autowired
    private TerritoryStatusRepository territoryStatusRepository;

    @Autowired
    private TerritoryRulesCheck territoryRulesCheck;

    public Boolean handle(Attempt attempt) {
        log.info("Determine if winner. So exciting...!");

        Optional<TerritoryStatus> optional = territoryStatusRepository.findById(Territory.valueOf(attempt.getTerritory().name()));
        if (!optional.isPresent()) {
            log.debug("Territory information is missing in DB.");
            throw new IllegalStateException("Territory information is missing.");
        }

        TerritoryStatus territoryStatus = optional.get();
        if (territoryRulesCheck.isWinner(territoryStatus)) {
            updateTerritoryStatus(territoryStatus);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private void updateTerritoryStatus(TerritoryStatus territoryStatus) {
        territoryStatus.incrementNumberOfWinners();
        territoryStatusRepository.save(territoryStatus);
    }
}
