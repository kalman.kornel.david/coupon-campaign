package com.exercise.campaign.service;

import com.exercise.campaign.TestConstants;
import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import com.exercise.campaign.repository.TerritoryStatusRepository;
import com.exercise.campaign.service.domain.CampaignException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class LimitHandlerTest {
    @Mock
    private TerritoryStatusRepository territoryStatusRepository;

    @Mock
    private TerritoryRulesCheck territoryRulesCheck;

    @InjectMocks
    private LimitHandler underTest;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenLimitsNotReached_thenWontThrowException() {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(10000)
                .numberOfWinnersToday(249)
                .numberOfWinnersOverall(4999)
                .build();

        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isDailyLimitReached(territoryStatus)).willReturn(false);
        given(territoryRulesCheck.isOverallLimitReached(territoryStatus)).willReturn(false);

        Assertions.assertDoesNotThrow(() -> underTest.handle(attempt));
    }

    @Test
    void whenDailyLimitReached_thenThrowCampaignException() {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(10000)
                .numberOfWinnersToday(250)
                .numberOfWinnersOverall(4999)
                .build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isDailyLimitReached(territoryStatus)).willReturn(true);
        given(territoryRulesCheck.isOverallLimitReached(territoryStatus)).willReturn(false);

        Assertions.assertThrows(CampaignException.class, () -> underTest.handle(attempt));
    }

    @Test
    void whenOverallLimitReached_thenThrowCampaignException() {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(10000)
                .numberOfWinnersToday(313)
                .numberOfWinnersOverall(5000)
                .build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isDailyLimitReached(territoryStatus)).willReturn(false);
        given(territoryRulesCheck.isOverallLimitReached(territoryStatus)).willReturn(true);

        Assertions.assertThrows(CampaignException.class, () -> underTest.handle(attempt));
    }

    @Test
    void whenLimitsNotReached_thenUpdateTerritoryStatusInDB() {
        final com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(10000)
                .numberOfWinnersToday(249)
                .numberOfWinnersOverall(4999)
                .build();

        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isDailyLimitReached(territoryStatus)).willReturn(false);
        given(territoryRulesCheck.isOverallLimitReached(territoryStatus)).willReturn(false);

        underTest.handle(attempt);

        verify(territoryStatusRepository, times(1)).save(territoryStatus);
    }

    @ParameterizedTest
    @MethodSource("generateTerritoryRulesCheckResults")
    void whenLimitsReached_thenWontUpdateTerritoryStatusInDB(boolean isDailyLimitReached, boolean isOverallLimitReached) {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(10000)
                .numberOfWinnersToday(249)
                .numberOfWinnersOverall(4999)
                .build();

        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isDailyLimitReached(territoryStatus)).willReturn(isDailyLimitReached);
        given(territoryRulesCheck.isOverallLimitReached(territoryStatus)).willReturn(isOverallLimitReached);

        Assertions.assertThrows(CampaignException.class, () -> underTest.handle(attempt));

        verify(territoryStatusRepository, never()).save(territoryStatus);
    }

    private static Stream<Arguments> generateTerritoryRulesCheckResults() {
        return Stream.of(
                Arguments.of(false, true),
                Arguments.of(true, false),
                Arguments.of(true, true)
        );
    }
}