package com.exercise.campaign.util;

import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.model.Territory;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.time.LocalDate;

public class RequestGenerator {
    private RequestGenerator() {
    }

    public static Attempt generateValidAttempt(Territory territory) {
        return Attempt.builder()
                .email(generateValidEmail())
                .couponCode(generateValidCouponCode())
                .territory(territory)
                .dateOfBirth(generateValidDateOfBirth())
                .address(generateAddress())
                .build();
    }

    private static String generateValidEmail() {
        return RandomStringUtils.random(RandomUtils.nextInt(5, 10), true, true) + "@mail.com";
    }

    private static String generateValidCouponCode() {
        return RandomStringUtils.random(10, true, true);
    }

    private static LocalDate generateValidDateOfBirth() {
        return LocalDate.now()
                .minusYears(RandomUtils.nextLong(15, 100))
                .plusMonths(RandomUtils.nextLong(1, 10))
                .minusDays(RandomUtils.nextLong(1, 25));
    }

    // For now just a string for performance testing purposes
    private static String generateAddress() {
        return RandomStringUtils.random(20, true, true);
    }
}
