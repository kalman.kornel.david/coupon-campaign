package com.exercise.campaign.validation.domain;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessError extends ApiError {
    private final String errorMessage;

    public BusinessError(String errorMessage) {
        super(HttpStatus.BAD_REQUEST);
        this.errorMessage = errorMessage;
    }
}
