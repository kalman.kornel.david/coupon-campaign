package com.exercise.campaign.service;

import com.exercise.campaign.TestConstants;
import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import com.exercise.campaign.repository.TerritoryStatusRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class WinnerHandlerTest {
    @Mock
    private TerritoryStatusRepository territoryStatusRepository;

    @Mock
    private TerritoryRulesCheck territoryRulesCheck;

    @InjectMocks
    private WinnerHandler underTest;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 40, 80, 120, 160, 200, 800, 1200, 2000, 3000, 4000})
    void whenCounterGivesWinner_thenHandleRequestAsWinner(int attemptCount) {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(attemptCount)
                .build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isWinner(territoryStatus)).willReturn(true);

        Boolean isWinner = underTest.handle(attempt);

        assertTrue(isWinner);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 7, 20, 39, 41, 42, 77, 79, 81, 999, 1001, 7999, 8001})
    void whenCounterDoesNotGiveWinner_thenHandleRequestAsNotWinner(int attemptCount) {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder()
                .numberOfAttempts(attemptCount)
                .build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isWinner(territoryStatus)).willReturn(false);

        Boolean isWinner = underTest.handle(attempt);

        assertFalse(isWinner);
    }

    @Test
    void whenCounterGivesWinner_thenUpdateTerritoryStatusInDB() {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder().build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isWinner(territoryStatus)).willReturn(true);

        underTest.handle(attempt);

        verify(territoryStatusRepository, times(1)).save(territoryStatus);
    }

    @Test
    void whenCounterDoesNotGivesWinner_thenUpdateTerritoryStatusInDB() {
        com.exercise.campaign.model.Attempt attempt = TestConstants.ATTEMPT;

        TerritoryStatus territoryStatus = TerritoryStatus.builder().build();
        given(territoryStatusRepository.findById(Territory.HUNGARY)).willReturn(Optional.of(territoryStatus));
        given(territoryRulesCheck.isWinner(territoryStatus)).willReturn(false);

        underTest.handle(attempt);

        verify(territoryStatusRepository, never()).save(territoryStatus);
    }
}
