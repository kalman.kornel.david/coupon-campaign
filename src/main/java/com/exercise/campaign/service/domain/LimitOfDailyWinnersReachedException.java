package com.exercise.campaign.service.domain;

public class LimitOfDailyWinnersReachedException extends CampaignException {
    public LimitOfDailyWinnersReachedException(String errorMessage) {
        super(errorMessage);
    }
}
