package com.exercise.campaign.service;

import com.exercise.campaign.TestConstants;
import com.exercise.campaign.domain.AttemptEntity;
import com.exercise.campaign.domain.UserEntity;
import com.exercise.campaign.model.Territory;
import com.exercise.campaign.repository.AttemptRepository;
import com.exercise.campaign.repository.UserRepository;
import com.exercise.campaign.service.domain.CampaignException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class CouponHandlerTest {
    @Mock
    private AttemptRepository attemptRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CouponHandler underTest;

    @Captor
    private ArgumentCaptor<AttemptEntity> attemptEntityCaptor;
    @Captor
    private ArgumentCaptor<UserEntity> userEntityCaptor;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCouponCodeAlreadyRedeemed_thenThrowCampaignException() {
        final com.exercise.campaign.model.Attempt attempt = com.exercise.campaign.model.Attempt.builder()
                .email(TestConstants.EMAIL)
                .couponCode(TestConstants.COUPON_CODE)
                .build();

        AttemptEntity attemptEntity = AttemptEntity.builder().build();
        given(attemptRepository.findById(TestConstants.COUPON_CODE)).willReturn(Optional.of(attemptEntity));

        Assertions.assertThrows(CampaignException.class, () -> underTest.handle(attempt));
    }

    @Test
    void whenCouponCodeNotYetRedeemed_thenWontThrowException() {
        final com.exercise.campaign.model.Attempt attempt = com.exercise.campaign.model.Attempt.builder()
                .email(TestConstants.EMAIL)
                .couponCode(TestConstants.COUPON_CODE)
                .territory(Territory.HUNGARY)
                .build();

        given(attemptRepository.findById(TestConstants.COUPON_CODE)).willReturn(Optional.empty());

        Assertions.assertDoesNotThrow(() -> underTest.handle(attempt));
    }

    @Test
    void whenCouponCodeAlreadyRedeemed_thenWontWriteDB() {
        final com.exercise.campaign.model.Attempt attempt = com.exercise.campaign.model.Attempt.builder()
                .email(TestConstants.EMAIL)
                .couponCode(TestConstants.COUPON_CODE)
                .territory(Territory.HUNGARY)
                .build();

        AttemptEntity attemptEntity = AttemptEntity.builder().build();
        given(attemptRepository.findById(TestConstants.COUPON_CODE)).willReturn(Optional.of(attemptEntity));

        final CampaignException campaignException = assertThrows(CampaignException.class, () -> underTest.handle(attempt));

        verify(attemptRepository, Mockito.times(1)).findById(attempt.getCouponCode());
        verifyNoMoreInteractions(attemptRepository);
    }

    @Test
    void whenCouponCodeNotYetRedeemed_thenSaveDataToDB() {
        final com.exercise.campaign.model.Attempt attempt = com.exercise.campaign.model.Attempt.builder()
                .email(TestConstants.EMAIL)
                .couponCode(TestConstants.COUPON_CODE)
                .territory(Territory.HUNGARY)
                .build();

        given(attemptRepository.findById(TestConstants.COUPON_CODE)).willReturn(Optional.empty());

        underTest.handle(attempt);

        verify(userRepository, Mockito.times(1)).save(userEntityCaptor.capture());
        verify(attemptRepository, Mockito.times(1)).save(attemptEntityCaptor.capture());
        final UserEntity userEntityCaptorValue = userEntityCaptor.getValue();
        final AttemptEntity attemptEntityCaptorValue = attemptEntityCaptor.getValue();
        assertEquals(attempt.getEmail(), userEntityCaptorValue.getEmail());
        assertEquals(attempt.getDateOfBirth(), userEntityCaptorValue.getDateOfBirth());
        assertEquals(attempt.getTerritory().name(), userEntityCaptorValue.getTerritory().name());
        assertEquals(attempt.getAddress(), userEntityCaptorValue.getAddress());
        assertEquals(attempt.getCouponCode(), attemptEntityCaptorValue.getCouponCode());
        assertEquals(userEntityCaptorValue, attemptEntityCaptorValue.getUser());
    }
}