package com.exercise.campaign.api;

import com.exercise.campaign.service.ServiceFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static com.exercise.campaign.TestConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AttemptController.class)
public class RequestValidationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ServiceFacade serviceFacade;

    @Test
    public void whenEmailIsInvalid_thenReturnStatus400() throws Exception {

        mockMvc.perform(post("/attempt")
                .param("email", INVALID_EMAIL)
                .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                .param("territory", VALID_TERRITORY)
                .param("couponCode", VALID_COUPON_CODE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenDateOfBirthIsInvalid_thenReturnStatus400() throws Exception {

        mockMvc.perform(post("/attempt")
                .param("email", VALID_EMAIL)
                .param("dateOfBirth", INVALID_DATE_OF_BIRTH)
                .param("territory", VALID_TERRITORY)
                .param("couponCode", VALID_COUPON_CODE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenCouponCodeIsInvalid_thenReturnStatus400() throws Exception {

        mockMvc.perform(post("/attempt")
                .param("email", VALID_EMAIL)
                .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                .param("territory", VALID_TERRITORY)
                .param("couponCode", INVALID_COUPON_CODE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenTerritoryIsInvalid_thenReturnStatus400() throws Exception {

        mockMvc.perform(post("/attempt")
                .param("email", VALID_EMAIL)
                .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                .param("territory", INVALID_TERRITORY)
                .param("couponCode", VALID_COUPON_CODE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenTerritoryIsMissing_thenReturnStatus400() throws Exception {

        mockMvc.perform(post("/attempt")
                .param("email", VALID_EMAIL)
                .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                .param("couponCode", VALID_COUPON_CODE))
                .andExpect(status().isBadRequest());
    }
}
