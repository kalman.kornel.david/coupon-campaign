package com.exercise.campaign.api;

import com.exercise.campaign.util.ResetDB;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static com.exercise.campaign.TestConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AttemptControllerIT {

    private static final String ENDPOINT = "/attempt";

    @Autowired
    private MockMvc mvc;

    @Test
    @ResetDB
    public void whenRequestHasValidFields_thenReturnStatus200() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE));
    }

    @Test
    @Sql({"/sql/insert_attempt.sql"})
    @ResetDB
    public void whenRequestContainsRedeemedCouponCode_thenReturnStatus400andMeaningfulErrorMessage() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.errorMessage", Matchers.is("This coupon code has already been redeemed.")));
    }

    @Test
    @Sql({"/sql/insert_territory_status_daily_limit.sql"})
    @ResetDB
    public void whenDailyLimitOfWinnersReached_thenReturnStatus2xxAndMeaningfulErrorMessage() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.errorMessage", Matchers.is("Maximum daily number of winners reached.")));
    }

    @Test
    @Sql({"/sql/insert_territory_status_overall_limit.sql"})
    @ResetDB
    public void whenOverallLimitOfWinnersReached_thenReturnStatus2xxAndMeaningfulErrorMessage() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.errorMessage", Matchers.is("Campaign ended in your territory.")));
    }

    @Test
    @Sql({"/sql/insert_territory_status_not_winner.sql"})
    @ResetDB
    @Sql(value = {"/sql/clean.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void whenWinnerCounterDoesNotYieldWinner_thenReturnStatus200AndProperResult() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.isWinner", Matchers.is(false)));
    }

    @Test
    @Sql({"/sql/insert_territory_status_winner.sql"})
    @ResetDB
    public void whenWinnerCounterYieldsWinner_thenReturnStatus200AndProperResult() throws Exception {
        mvc.perform(
                post(ENDPOINT)
                        .param("email", VALID_EMAIL)
                        .param("dateOfBirth", VALID_DATE_OF_BIRTH)
                        .param("territory", VALID_TERRITORY)
                        .param("couponCode", VALID_COUPON_CODE)
                        .param("address", VALID_ADDRESS)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.isWinner", Matchers.is(true)));
    }
}
