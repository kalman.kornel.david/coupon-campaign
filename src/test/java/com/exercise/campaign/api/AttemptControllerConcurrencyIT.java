package com.exercise.campaign.api;

import com.exercise.campaign.domain.Territory;
import com.exercise.campaign.domain.TerritoryStatus;
import com.exercise.campaign.model.Attempt;
import com.exercise.campaign.repository.TerritoryStatusRepository;
import com.exercise.campaign.util.RequestGenerator;
import com.exercise.campaign.util.ResetDB;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AttemptControllerConcurrencyIT {
    private static final String ENDPOINT = "/attempt";

    @Autowired
    private MockMvc mvc;

    @Autowired
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    List<FutureTask<ResponseEntity>> taskList;

    @Autowired
    TerritoryStatusRepository territoryStatusRepository;

    /*
     * Generates test casers in ascending order of the number of attempts.
     * Daily limit of winners is reached at the 80th attempt (check application-test.properties for territory rules),
     * after that the number of winner would not increase.
     */
    private static Stream<Arguments> generateScenariosForConcurrentRequests() {
        return Stream.of(
                Arguments.of(7, 7, 0, 0),
                Arguments.of(8, 8, 1, 1),
                Arguments.of(15, 15, 1, 1),
                Arguments.of(16, 16, 2, 2),
                Arguments.of(64, 64, 8, 8),
                Arguments.of(80, 80, 10, 10),
                Arguments.of(88, 80, 10, 10),
                Arguments.of(120, 80, 10, 10)
        );
    }

    @ParameterizedTest
    @MethodSource("generateScenariosForConcurrentRequests")
    @ResetDB
    public void whenMultipleRequestsArriveForTheSameTerritory_thenDatabaseShouldStoreCorrectValues(
            int numberOfAttempts, int numberOfRegisteredAttempts, int numberOfWinnersToday, int numberOfWinnersOverall) {

        // Generate request list
        taskList = new ArrayList<>(numberOfAttempts);
        for (int i = 0; i < numberOfAttempts; i++) {
            taskList.add(new FutureTask(new MyTask(), ResponseEntity.ok().build()));
        }

        // Send requests
        taskList.forEach(FutureTask::run);

        // Wait all request to finish
        final List<Object> resultList = taskList.stream()
                .map(futureTask -> {
                    try {
                        return futureTask.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());

        // Check results
        final Optional<TerritoryStatus> territoryStatusOptional = territoryStatusRepository.findById(Territory.HUNGARY);
        Assertions.assertTrue(territoryStatusOptional.isPresent());

        final TerritoryStatus territoryStatus = territoryStatusOptional.get();
        Assertions.assertEquals(numberOfRegisteredAttempts, territoryStatus.getNumberOfAttempts());
        Assertions.assertEquals(numberOfWinnersToday, territoryStatus.getNumberOfWinnersToday());
        Assertions.assertEquals(numberOfWinnersOverall, territoryStatus.getNumberOfWinnersOverall());
    }

    class MyTask implements Runnable {
        private final Attempt attempt;

        public MyTask() {
            attempt = RequestGenerator.generateValidAttempt(com.exercise.campaign.model.Territory.HUNGARY);
        }

        @Override
        public void run() {
            try {
                mvc.perform(
                        post(ENDPOINT)
                                .param("email", attempt.getEmail())
                                .param("couponCode", attempt.getCouponCode())
                                .param("territory", attempt.getTerritory().toString())
                                .param("dateOfBirth", attempt.getDateOfBirth().toString())
                                .param("address", attempt.getAddress())
                ).andReturn();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
